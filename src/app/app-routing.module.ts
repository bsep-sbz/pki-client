import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppGuard } from './app.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AppGuard],
    children: [
      { path: 'certs', loadChildren: './certs/certs.module#CertsModule' }
    ],
    data: { roles: ['Security Admin'] }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
