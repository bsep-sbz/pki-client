import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { CertService } from "../../services/cert.service";
import countries from './country-codes'
import countryCodes from './country-codes';

@Component({
  selector: "app-issuance",
  templateUrl: "./issuance.component.html",
  styleUrls: ["./issuance.component.css"]
})
export class IssuanceComponent implements OnInit {
  constructor(private certService: CertService) {}

  private issuerControl = new FormControl();
  private countryControl = new FormControl();

  private issuers = [["signing-ca.megatravel.com"]];
  private countries = countryCodes;

  private generateKeys = true;
  private certAuthority = false;
  private keyUsage = [
    { key: "digitalSignature", text: "Digital signature", value: false },
    { key: "contentCommitment", text: "Non-repudiation", value: false },
    { key: "keyEncipherment", text: "Key encipherment", value: false },
    { key: "dataEncipherment", text: "Data encipherment", value: false },
    { key: "keyAgreement", text: "Key agreement", value: false },
    { key: "keyCertSign", text: "Certificate signing", value: false },
    { key: "crlSign", text: "CRL signing", value: false },
    { key: "encipherOnly", text: "Encipher only", value: false },
    { key: "decipherOnly", text: "Decipher only", value: false }
  ];
  private extendedKeyUsage = [
    { key: "serverAuth", text: "TLS Web server authentication", value: false },
    { key: "clientAuth", text: "TLS Web client authentication", value: false },
    { key: "codeSigning", text: "Sign executable code", value: false },
    { key: "emailProtection", text: "Email protection", value: false },
    { key: "timeStamping", text: "Timestamping", value: false },
    { key: "ocspSigning", text: "OCSP signing", value: false },
    { key: "anyExtendedKeyUsage", text: "Any extended usage", value: false }
  ];

  private nameConstraintsPermitted = [];
  private nameConstraintsExcluded = [];
  private authorityInformationAccess = [];
  private alternativeNames = [];

  private publicKey = null;

  private addNameExcluded() {
    this.nameConstraintsExcluded.push({
      type: null,
      value: ""
    });
  }

  private addNamePermitted() {
    this.nameConstraintsPermitted.push({
      type: null,
      value: ""
    });
  }

  private addAlternativeName() {
    this.alternativeNames.push({
      type: null,
      value: ""
    });
  }

  private addAuthorityInfo() {
    this.authorityInformationAccess.push({
      method: null,
      value: {
        type: null,
        value: ""
      }
    });
  }

  private handlePublicKeyUpload(event) {
    const file = event.target.files[0];

    const reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = event => {
      this.publicKey = btoa(event.target['result']);
    };
  }

  private subject = {
    country: "",
    organization: "",
    organizationalUnit: "",
    commonName: ""
  };

  private submitForm() {
    const data = {
      subject: this.subject,
      certificationAuthority: this.certAuthority,
      generateKeys: this.generateKeys,
      keyUsage: this.keyUsage.filter(key => key.value).map(key => key.key),
      extendedKeyUsage: this.extendedKeyUsage
        .filter(key => key.value)
        .map(key => key.key),

      nameConstraints: {
        permited: this.nameConstraintsPermitted,
        excluded: this.nameConstraintsExcluded
      },
      subjectAlternativeName: this.alternativeNames,
      authorityInformationAccess: this.authorityInformationAccess.map(x => ({
        ...x,
        value: {
          ...x.value,
          value: x.value.value[0]
        }
      }))
    };

    this.certService.issueCert(data).subscribe(response => {
      console.log(response);
    });
  }

  private fetchCAs() {
    // TODO
  }

  ngOnInit() {
    this.fetchCAs();
  }
}
