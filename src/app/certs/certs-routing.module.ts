import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CertsComponent } from './certs.component';
import { IssuanceComponent } from './issuance/issuance.component';
import { TemplateComponent } from './template/template.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '',
    component: CertsComponent,
    children: [
      {
        path: 'issuance',
        component: IssuanceComponent
      },
      {
        path: 'list',
        component: ListComponent
      },
      {
        path: 'templates',
        component: TemplateComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertsRoutingModule { }
