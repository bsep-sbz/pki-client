import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertsRoutingModule } from './certs-routing.module';
import { IssuanceComponent } from './issuance/issuance.component';
import { CertsComponent } from './certs.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TemplateComponent } from './template/template.component';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [IssuanceComponent, CertsComponent, TemplateComponent, ListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,

    CertsRoutingModule,
  ]
})
export class CertsModule { }
