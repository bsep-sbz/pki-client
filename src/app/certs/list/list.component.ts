import { Component } from "@angular/core";
import { CertService } from "../../services/cert.service";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent {
  constructor(private certService: CertService) {}

  private certs = [
    // {
    //   serialNumber: 123432333,
    //   commonName: "Test Cert",
    //   createdAt: "2008-09-15T15:53:00",
    //   issuer: "MegaTravel CA 1"
    // },
    // {
    //   serialNumber: 12312423,
    //   commonName: "Test Cert",
    //   createdAt: "2008-09-15T15:53:00",
    //   issuer: "MegaTravel CA 1"
    // },
    // {
    //   serialNumber: 123432423,
    //   commonName: "Test Cert",
    //   createdAt: "2008-09-15T15:53:00",
    //   issuer: "MegaTravel CA 1"
    // }
  ];

  displayedColumns: string[] = [
    "serialNumber",
    "commonName",
    "issuer",
    "revoke"
  ];
  // dataSource = this.certs;

  private revokeCert(serialNumber) {
    // setTimeout(() => {
    //   this.certs = this.certs.filter(
    //     cert => cert.serialNumber !== serialNumber
    //   );
    // }, 400);
    this.certService.revokeCert(serialNumber).subscribe(() => {
      this.certs = this.certs.filter(
        cert => cert.serialNumber !== serialNumber
      );
    });
  }
}
