import { Component } from "@angular/core";
import { CertService } from "../../services/cert.service";

@Component({
  selector: "app-template",
  templateUrl: "./template.component.html",
  styleUrls: ["./template.component.css"]
})
export class TemplateComponent {
  constructor(private certService: CertService) {}

  private content = '';

  private submitForm() {
    this.certService.createTemplate({
      content: this.content
    })
    .finally(() => {
      alert('Template created');
    });
  }

}
