import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pki-client';

  private navlinks = [
    {
      text: 'Certification',
      sublinks: [
        { path: '/certs/issuance', text: 'Issuance' },
        { path: '/certs/list', text: 'List Certs' },
        { path: '/certs/templates', text: 'Create Template' },
      ],
    },
  ]

  constructor(private router: Router) {

  }

  private navigate(path): void {
    this.router.navigate([path]);
  }
}
