import { BrowserModule } from '@angular/platform-browser';
import { environment } from './../environments/environment';
import { ApiHttpInterceptor } from './services/api-http-interceptor';
import {
  NgModule,
  APP_INITIALIZER
} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {
  KeycloakAngularModule,
  KeycloakService
} from 'keycloak-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';


function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => keycloak.init();
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    KeycloakAngularModule,

    MaterialModule,

    AppRoutingModule,
  ],
  providers: [
    { provide: 'BASE_API_URL', useValue: environment.baseApiUrl },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiHttpInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
