import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import axios from 'axios';

const BASE_JAVA_URL = 'http://localhost:9876';

const ENDPOINTS = {
    GET_ISSUERS: '/issuers',
    GET_CERTIFICATES: '/certificates',
    ISSUE_CERT: '/certificates/issue',
    REVOKE_CERT: '/certificates/revoke',
    CREATE_TEMPLATE: '/templates'
}

@Injectable({
  providedIn: 'root'
})
export class CertService {
  constructor(private http: HttpClient) { }

  getIssuers() {
    return this.http.get(ENDPOINTS.GET_ISSUERS);
  }

  issueCert(data) {
    return this.http.post(ENDPOINTS.ISSUE_CERT, data);
  }

  getCerts() {
    return this.http.get(ENDPOINTS.GET_CERTIFICATES);
  }

  revokeCert(serialNumber) {
    return this.http.post(ENDPOINTS.REVOKE_CERT + `/${serialNumber}`, {});
  }

  createTemplate(data) {
    return axios.post(BASE_JAVA_URL + ENDPOINTS.CREATE_TEMPLATE, data);
  }

}