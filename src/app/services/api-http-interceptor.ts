import {
  Injectable,
  Inject,
} from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {
  constructor(@Inject('BASE_API_URL') private baseUrl: string) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const newRequest = { url: `${this.baseUrl}${req.url}` };
    let token = localStorage.getItem('token');

    if (token) {
      token = token.substr(1, token.length - 2);
      Object.assign(newRequest, {
        headers: req.headers.set('Authorization', `Bearer ${token}`)
      });
    }
    const cloned = req.clone(newRequest);
    return next.handle(cloned);
  }
}
